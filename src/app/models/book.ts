export interface Book{
    id: number;
    tittle: string,
    description: string,
    image: string,
    publication: string,
    author: string,
    matter: string,
    synopsis: string,
}